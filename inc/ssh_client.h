/*
 * ssh_client.h
 *
 *  Created on: Jul 19, 2017
 *      Author: i_komarov
 */

#ifndef SSH_CLIENT_H_
#define SSH_CLIENT_H_

#include <dlog.h>
#include <libssh/libssh.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "tizenssh"

#if !defined(PACKAGE)
#define PACKAGE "net.styleru.ikomarov.tizenssh"
#endif

/**
 * Prototype declaring the callback with no return type and no arguments.
 *
 * @return void
 * */
typedef void (*callback)(

);
/**
 * Prototype declaring the callback with no return type that might be used for
 * listening for session creation result.
 *
 * @param session the instance of ssh_session that was successfully created
 *
 * @return void
 * */
typedef void (*ssh_session_callback)(
		ssh_session session
);
/**
 * Prototype declaring the callback that might be used for notifying user about
 * an unstandard case.
 *
 * @param case_code an integer representing the code indicating particular case
 * @param message   message that should be shown
 *
 * @return void
 * */
typedef void  (*ssh_session_user_notification_callback)(
		const int *case_code,
		const char *message
);

/**
 * Prototype declaring the callback that might be used for interacting with user
 * during some unstandard case and get his desire.
 *
 * @param case_code the   an integer representing the code indicating particular case
 * @param response_length maximum length of user response
 * @param message         message that should be shown
 *
 * @return string representation of user response
 * */
typedef char *(*ssh_session_user_interaction_callback)(
		const int *case_code,
		const int *response_length,
		const char *message
);

/**
 * Function for creating a session and setting up its general information
 * about the host session is being connected with.
 *
 * @param host
 * @param port
 *
 * @return an instance of ssh_session or NULL
 * */
ssh_session ssh_session_create(
		const char *host,
		const int port
);
/**
 * Function for creating a session and setting up its general information
 * about the host session is being connected with.
 *
 * @param host
 * @param port
 * @param success_callback the callback function that would be triggered on session creation success
 * @param failure_callback the callback function that would be triggered on session creation failure
 *
 * @return void
 * */
void ssh_session_create_with_callbacks(
		const char *host,
		const int *port,
		const ssh_session_callback success_callback,
		const callback failure_callback
);
/**
 * Function for verifying the known host
 *
 * @param session the session to verify data of
 * @param knownhost_verification_success_callback the callback to handle success e.g. host is known
 * @param knownhost_verification_failed_callback the callback to handle failure e.g. host is not known
 * @param user_interaction_callback the callback to interact with user, when his desire is needed to continue
 *
 * @return void
 * */
void ssh_session_verify_knownhost(
		ssh_session session,
		ssh_session_callback knownhost_verification_success_callback,
		ssh_session_callback knownhost_verification_failed_callback,
		ssh_session_user_interaction_callback user_interaction_callback
);
/**
 * Function for releasing an ssh_session instance.
 *
 * @param session the session to release
 *
 * @return void
 * */
void ssh_session_release(
		ssh_session session
);

#endif /* SSH_CLIENT_H_ */
