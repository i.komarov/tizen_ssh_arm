/*
 * tizenssh.h
 *
 *  Created on: Jul 19, 2017
 *      Author: i_komarov
 */

#ifndef __tizenssh_H__
#define __tizenssh_H__

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "tizenssh"

#if !defined(PACKAGE)
#define PACKAGE "net.styleru.ikomarov.tizenssh"
#endif

#endif /* __tizenssh_H__ */
