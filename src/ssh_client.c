/*
 * ssh_client.c
 *
 *  Created on: Jul 19, 2017
 *      Author: i_komarov
 */

#include "ssh_client.h"

ssh_session ssh_session_create(const char *host, const int port) {
	ssh_session session = ssh_new();
	if(session == NULL) {
		dlog_print(DLOG_INFO, LOG_TAG, "ssh_session_create(); session creation failed: ssh_new(); returned NULL");
		return NULL;
	}

	ssh_options_set(session, SSH_OPTIONS_HOST, host);
	ssh_options_set(session, SSH_OPTIONS_PORT, &port);

	int status_code = ssh_connect(session);
	if(status_code != SSH_OK) {
		dlog_print(DLOG_INFO, LOG_TAG, "ssh_session_create(); session connection failed: ssh_connect(); returned not SSH_OK");
		return NULL;
	}

	dlog_print(DLOG_INFO, LOG_TAG, "ssh_session_create(); session created and connection established successfully");
	return session;
}

void ssh_session_release(ssh_session session) {
	dlog_print(DLOG_INFO, LOG_TAG, "ssh_session_release(); session releasing started");
	if(session != NULL) {
		  ssh_disconnect(session);
		  ssh_free(session);
			dlog_print(DLOG_INFO, LOG_TAG, "ssh_session_release(); session successfully_released");
	}
}


